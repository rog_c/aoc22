# Advent of Code 2022

My Rust solves for AOC 2022.

I'm using this year as an opportunity to learn Rust. My goal is to make this repo as readable as poossible so I can reference it as I continue to learn.

## Usage

cd into each day's directory and simply run:

```bash
cargo run --release
```
