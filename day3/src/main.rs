use itertools::Itertools;

fn p1(line: &str) -> u32
{
    let mut priority: u32 = 0;
    let mid = line.chars().count()/2;

    let (s1, s2) = line.split_at(mid);
    let vec1: Vec<char> = s1.chars().sorted().collect();
    let vec2: Vec<char> = s2.chars().sorted().collect();


    let mut common: char = '-';
    let mut i: usize = 0;
    let mut j:usize = 0;

    while i < mid && j < mid
    {
        let c1 = vec1[i];
        let c2 = vec2[j];

        if c1 == c2
        {
            common = c1;
            break;
        }
        else if c1 > c2
        {
            j += 1;
        }
        else
        {
            i += 1;
        }
    }

    if common.is_ascii_lowercase()
    {
        priority = common as u32 - 96;
    }
    else if common.is_uppercase()
    {
        priority = common as u32 - 38;
    }

    return priority;
}

fn p2() -> u32
{
    let input: Vec<&str> = include_str!("../inputs/input.txt").trim().lines().collect();
    let mut priority: u32 = 0;

    let total_inputs: usize = input.len();
    let mut curr_group: usize = 0;

    while curr_group < total_inputs
    {
        let mut elf1: Vec<char> = input[curr_group].chars().sorted().collect();
        let mut elf2: Vec<char> = input[curr_group + 1].chars().sorted().collect();
        let mut elf3: Vec<char> = input[curr_group + 2].chars().sorted().collect();

        elf1.dedup();
        elf2.dedup();
        elf3.dedup();

        let mut common: char = '-';
        let mut i: usize = 0;
        let mut j:usize = 0;
        let mut k:usize = 0;

        while i < elf1.len() && j < elf2.len() && k < elf3.len()
        {
            let c1 = elf1[i];
            let c2 = elf2[j];
            let c3 = elf3[k];

            if c1 == c2
            {
                if c1 == c3
                {
                    common = c1;
                    break;
                }
                else if c1 > c3
                {
                    k += 1;
                }
                else
                {
                    i += 1;
                }
            }
            else if c1 > c2
            {
                j += 1;
            }
            else
            {
                i += 1;
            }
        }

        if common.is_ascii_lowercase()
        {
            priority += common as u32 - 96;
        }
        else if common.is_uppercase()
        {
            priority += common as u32 - 38;
        }
        curr_group += 3;
    }


    return priority;
}

fn main() {
    let input: &str = include_str!("../inputs/input.txt");
    let mut p1_total: u32 = 0;

    for line in input.trim().lines()
    {
        p1_total += p1(line);
    }

    let p2_total: u32 = p2();

    println!("Part 1 Total: {p1_total}");
    println!("Part 2 Total: {p2_total}");
}
