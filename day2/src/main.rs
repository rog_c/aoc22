static LOST: u32 = 0;
static DRAW: u32 = 3;
static WIN: u32 = 6;
static ROCK: u32 = 1;
static PAPER: u32 = 2;
static SCISSORS: u32 = 3;


fn calculate_score_p1(op_choice: char, my_choice: char) -> u32
{
    let mut score: u32 = 0;

    score += match my_choice
    {
        'X' => ROCK + match op_choice
        {
            'A' => DRAW,
            'B' => LOST,
            'C' => WIN,
            _ => panic!()
        },
        'Y' => PAPER + match op_choice
        {
            'A' => WIN,
            'B' => DRAW,
            'C' => LOST,
            _ => panic!()
        },
        'Z' => SCISSORS + match op_choice
        {
            'A' => LOST,
            'B' => WIN,
            'C' => DRAW,
            _ => panic!()
        },
        _ => panic!()
    };

    return score;
}

fn calculate_score_p2(op_choice: char, outcome: char) -> u32
{
    let mut score: u32 = 0;

    score += match outcome
    {
        'X' => LOST + match op_choice
        {
            'A' => SCISSORS,
            'B' => ROCK,
            'C' => PAPER,
            _ => panic!()
        },
        'Y' => DRAW + match op_choice
        {
            'A' => ROCK,
            'B' => PAPER,
            'C' => SCISSORS,
            _ => panic!()
        },
        'Z' => WIN + match op_choice
        {
            'A' => PAPER,
            'B' => SCISSORS,
            'C' => ROCK,
            _ => panic!()
        },
        _ => panic!()
    };

    return score;
}

fn main() {
    let mut p1_total: u32 = 0;
    let mut p2_total: u32 = 0;
    let input: &str = include_str!("../inputs/input.txt");

    for line in input.trim().lines()
    {
        let op_choice: char = line.chars().nth(0).unwrap();
        let response: char = line.chars().nth(2).unwrap();
        p1_total += calculate_score_p1(op_choice, response);
        p2_total += calculate_score_p2(op_choice, response);
    }

    println!("Part 1 Total: {p1_total}");
    println!("Part 2 Total: {p2_total}");
}
