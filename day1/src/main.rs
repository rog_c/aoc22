fn get_largest() -> (i32, i32)
{
    let input = include_str!("../inputs/input.txt");
    let mut maxes = vec![];
    let mut count = 0;

    for line in input.lines()
    {
        if line.is_empty()
        {
            maxes.push(count);
            count = 0;
        }
        else
        {
            count += line.parse::<i32>().expect("Could not parse");
        }
    }
    maxes.sort();
    maxes.reverse();

    (maxes[0], maxes[0..3].iter().sum())
}
fn main()
{
    let (part1, part2) = get_largest();
    println!("Part 1 ans: {part1}");
    println!("Part 2 ans: {part2}");
}