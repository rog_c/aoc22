fn p1(curr_line: &str) -> u32
{
    let mut full_contain: u32 = 0;

    let vec1: Vec<&str> = curr_line.split(",").collect();

    let range1_str: Vec<&str> = vec1[0].split("-").collect();
    let range2_str: Vec<&str> = vec1[1].split("-").collect();


    let x1: u32 = range1_str[0].parse::<u32>().expect("Unable to parse");
    let y1: u32 = range1_str[1].parse::<u32>().expect("Unable to parse");
    let x2: u32 = range2_str[0].parse::<u32>().expect("Unable to parse");
    let y2: u32 = range2_str[1].parse::<u32>().expect("Unable to parse");


    if (x1 >= x2 && y1 <= y2) || (x2 >= x1 && y2 <= y1)
    {

        full_contain += 1;
    }

    return full_contain;
}

fn p2(curr_line: &str) -> u32
{
    let mut contains: u32 = 0;

    let vec1: Vec<&str> = curr_line.split(",").collect();

    let range1_str: Vec<&str> = vec1[0].split("-").collect();
    let range2_str: Vec<&str> = vec1[1].split("-").collect();


    let x1: u32 = range1_str[0].parse::<u32>().expect("Unable to parse");
    let y1: u32 = range1_str[1].parse::<u32>().expect("Unable to parse");
    let x2: u32 = range2_str[0].parse::<u32>().expect("Unable to parse");
    let y2: u32 = range2_str[1].parse::<u32>().expect("Unable to parse");


    if (y1 >= x2 && x1 <= x2) || (y2 >= x1 && x2 <= x1)
    {

        contains += 1;
    }

    return contains;
}

fn main() {
    let input: &str = include_str!("../inputs/input.txt");
    let mut p1_total: u32 = 0;
    let mut p2_total: u32 = 0;

    for line in input.trim().lines()
    {
        p1_total += p1(line);
        p2_total += p2(line);
    }

    println!("Part 1 Total = {p1_total}");
    println!("Part 2 Total = {p2_total}");
}
