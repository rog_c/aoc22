use scan_fmt::scan_fmt;
use std::collections::VecDeque;

fn p1(lines: Vec<&str>, mut vec: Vec<VecDeque<char>>)
{
    for i in 0..lines.len()
    {
        let (x, mut y, mut z) = scan_fmt!(lines[i], "move {d} from {d} to {d}", usize, usize, usize).unwrap();
        y -= 1;
        z -= 1;
        for _j in 0..x
        {
            let tmp: char = vec[y].pop_back().unwrap();
            vec[z].push_back(tmp);
        }
    }

    print!("Part 1 Answer: ");
    for i in 0..vec.len()
    {
        let c: char = vec[i].pop_back().unwrap();
        print!("{c}");
    }
    println!("");
}

fn p2(lines: Vec<&str>, mut vec: Vec<VecDeque<char>>)
{
    for i in 0..lines.len()
    {
        let (x, mut y, mut z) = scan_fmt!(lines[i], "move {d} from {d} to {d}", usize, usize, usize).unwrap();
        y -= 1;
        z -= 1;
        let mut tmp_vec: VecDeque<char> = VecDeque::from_iter([]);
        for _j in 0..x
        {
            let tmp: char = vec[y].pop_back().unwrap();
            tmp_vec.push_front(tmp);

        }
        vec[z].append(&mut tmp_vec);
    }

    print!("Part 2 Answer: ");
    for i in 0..vec.len()
    {
        let c: char = vec[i].pop_back().unwrap();
        print!("{c}");
    }
    println!("");
}

fn print_stacks(vec: Vec<VecDeque<char>>)
{
    for i in 0..9
    {
        print!("{:?}: ", i + 1);

        for j in 0..vec[i].len()
        {
            let curr = vec[i][j];
            print!("{curr} ");
        }
        println!("");
    }
}

fn main()
{
    let input: &str = include_str!("../inputs/input.txt");

    let mut vec: Vec<VecDeque<char>> = vec![];

    for _i in 0..9
    {
        let new_vec: VecDeque<char> = VecDeque::from_iter([]);
        vec.push(new_vec);
    }

    let lines: Vec<&str> = input.split("\n").collect();

    for i in 0..8
    {
        let line: Vec<char> = lines[i].chars().collect();
        let mut index: usize = 1;

        while index < line.len()
        {
            let curr_item: char = line[index];
            if curr_item != ' '
            {
                let curr_index = index / 4;
                vec[curr_index].push_front(curr_item);
            }
            index += 4;
        }
    }

    print_stacks(vec.clone());

    let mut instructions: Vec<&str> = vec![];
    for i in 10..lines.len()
    {
        instructions.push(lines[i]);
    }

    p1(instructions.clone(), vec.clone());
    p2(instructions.clone(), vec.clone());

}
