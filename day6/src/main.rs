use itertools::Itertools;

fn p1(input: &str)
{
    let result: u32 = input.chars().collect::<Vec<char>>()[0..input.len()]
        .windows(4)
        .find_position(|window| (*window).iter().all_unique())
        .unwrap().0 as u32;

    println!("Part 1 result: {:?}", Some(result + 4));
}

fn p2(input: &str)
{
    let result: u32 = input.chars().collect::<Vec<char>>()[0..input.len()]
        .windows(14)
        .find_position(|window| (*window).iter().all_unique())
        .unwrap().0 as u32;

    println!("Part 2 result: {:?}", Some(result + 14));
}

fn main()
{
    let input: &str = include_str!("../inputs/input.txt");

    p1(input);
    p2(input);
}
